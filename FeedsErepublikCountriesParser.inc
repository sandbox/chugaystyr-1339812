<?php
/**
 * @file
 *  Feeds parser class for eRepublik countries list.

/**
 * Parses XML file.
 */
class FeedsErepublikCountriesParser extends FeedsParser {

  public function parse(FeedsImportBatch $batch, FeedsSource $source) {
    // Get the XML
    $string = $batch->getRaw();
    $objFeed = simplexml_load_string($string);

    // Parse SimpleXMLObject
    $items = array();
    foreach ($objFeed->country as $objItem) {
      $items[] = array(
        'country_code' => (string) $objItem->code,
        'country_average_citizen_level' => (string) $objItem->{'average-citizen-level'},
        'country_citizen_count' => (string) $objItem->{'citizen-count'},
        'country_continent' => (string) $objItem->continent,
        'country_citizen_fee' => (string) $objItem->{'citizen-fee'},
        'country_name' => (string) $objItem->name,
        'country_currency' => (string) $objItem->currency,
        'country_id' => (string) $objItem->id,
        'country_region_count' => (string) $objItem->{'region-count'},
      );
      //// Populate the FeedsImportBatch object with the parsed results.
      $batch->title = 'Countries';
      $batch->items = $items;
    }
  }

  /**
   * Add the mapping sources provided by this parser.
   */
  public function getMappingSources() {

    return array(
      'country_code' => array(
      'name' => t('Country: code'),
      'description' => t('Country:code field.'),
      ),
      'country_average_citizen_level' => array(
      'name' => t('Country: average citizen level'),
      'description' => t('Country:average citizen level field.'),
      ),
      'country_citizen_count' => array(
      'name' => t('Country: citizen count'),
      'description' => t('Country:citizen count field.'),
      ),
      'country_continent' => array(
      'name' => t('Country: continent'),
      'description' => t('Country:continent field.'),
      ),
      'country_citizen_fee' => array(
      'name' => t('Country: citizen fee'),
      'description' => t('Country:citizen fee field.'),
      ),
      'country_name' => array(
      'name' => t('Country: name'),
      'description' => t('Country:name field.'),
      ),
      'country_currency' => array(
      'name' => t('Country: currency'),
      'description' => t('Country:currency field.'),
      ),
      'country_id' => array(
      'name' => t('Country: id'),
      'description' => t('Country:id field.'),
      ),
      'country_region_count' => array(
      'name' => t('Country: region count'),
      'description' => t('Country:region count field.'),
      )
    );
  }
}
