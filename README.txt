Browser game eRepublik.com integration with Drupal Feeds module through erepublik's feeds eAPI. With this module you can parse eRepublik's feeds to drupal nodes for the game objects: countries (ToDo: citizens, wars, industries etc).

Installation and configuration:
--------------------------------
1. Download and install the module.
2. Go to Administer -> Site Building -> Feed importers.
3. Find the set of eRepublik parsers and configure the importer like any another feeds importer.
